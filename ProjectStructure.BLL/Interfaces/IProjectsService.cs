﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IProjectsService
    {
        public IEnumerable<ProjectDTO> GetProjects();
        ProjectDTO FindProjectById(int id);
        void CreateProject(ProjectDTO item);
        void RemoveProject(int id);
        void UpdateProject(ProjectDTO item);
    }
}
