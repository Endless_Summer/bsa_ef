﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITeamsService
    {
        public IEnumerable<TeamDTO> GetTeams();
        TeamDTO FindTeamById(int id);
        void CreateTeam(TeamDTO item);
        void RemoveTeam(int id);
        void UpdateTeam(TeamDTO item);
    }
}
