﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IUsersService
    {
        public IEnumerable<UserDTO> GetUsers();
        UserDTO FindUserById(int id);
        void CreateUser(UserDTO item);
        void RemoveUser(int id);
        void UpdateUser(UserDTO item);
    }
}
