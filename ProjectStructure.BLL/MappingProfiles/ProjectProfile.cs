﻿using AutoMapper;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>()
            .ForMember(dest => dest.AuthorId,
            opt => opt.MapFrom(src => src.Author.Id))
            .ForMember(dest => dest.TeamId,
            opt => opt.MapFrom(src => src.Team.Id))

            .ReverseMap()
            //.ForMember(dest => dest.Author,
            //opt => opt.MapFrom(src => new User()))
            //.ForMember(dest => dest.Team,
            //opt => opt.MapFrom(src => new Team()))
            ;
        }
    }
}
