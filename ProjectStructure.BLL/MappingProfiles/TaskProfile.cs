﻿using AutoMapper;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>()
                .ForMember(dest => dest.ProjectId,
                opt => opt.MapFrom(src => src.Project.Id))
                .ForMember(dest => dest.PerformerId,
                opt => opt.MapFrom(src => src.Performer.Id))

            .ReverseMap()
                //.ForMember(dest => dest.Project,
                //opt => opt.MapFrom(src => new Project()))
                //.ForMember(dest => dest.Performer,
                //opt => opt.MapFrom(src => new User())

            ;
        }
    }
}
