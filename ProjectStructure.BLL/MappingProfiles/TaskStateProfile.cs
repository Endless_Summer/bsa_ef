﻿using AutoMapper;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class TaskStateProfile : Profile
    {
        public TaskStateProfile()
        {
            CreateMap<TaskState, TaskStateDTO>()
                .ReverseMap()
            ;
        }
    }
}
