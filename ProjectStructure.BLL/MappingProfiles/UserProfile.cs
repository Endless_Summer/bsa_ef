﻿using AutoMapper;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.TeamId,
                opt => opt.MapFrom(src => src.Team.Id))
                .ReverseMap();
            //.ForMember(dest => dest.Team,
            //opt => opt.MapFrom(src => new Team())


        }


    }
}
