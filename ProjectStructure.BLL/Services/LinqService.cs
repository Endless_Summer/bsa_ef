﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class LinqService : ILinqService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;


        public LinqService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }



        public IEnumerable<ProjectUserTasksCountDTO> GetProjectUserTasksCount(int userId) // Task 1
        {
            var result = db.Projects.Get(p => p.AuthorId == userId)
                .GroupJoin(db.Tasks.Get(), p => p.Id, t => t.ProjectId, (p, t) => new ProjectUserTasksCountDTO
                {
                    Project = _mapper.Map<ProjectDTO>(p),
                    UserTasksCount = t.Count()
                });

            return result;
        }

        public IEnumerable<TaskDTO> GetUserTasks(int userId) // Task 2
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(db.Tasks.Get(t => t.PerformerId == userId && t.Name.Length < 45));
        }

        public IEnumerable<TaskShortDTO> GetFinishedTasksForUser(int userId) // Task 3
        {
            var result = db.Tasks.Get(t => t.PerformerId == userId && t.FinishedAt.Year == DateTime.Now.Year && t.State == 2)
                .Select(t => new TaskShortDTO
                {
                    Id = t.Id,
                    Name = t.Name
                });

            return result;
        }

        public IEnumerable<TeamUsersDTO> GetAgeLimitTeams() // Task 4
        {

            var result = db.Users.Get()
                .Where(u => u.Birthday.Year < DateTime.Now.Year - 10)
                .Join(db.Teams.Get(), u => u.Team?.Id, t => t.Id, (u, t) => new { User = u, Team = t })
                .GroupBy(x => x.Team.Id)
                .Select(x => new TeamUsersDTO
                {
                    Id = x.First().Team.Id,
                    Name = x.First().Team.Name,
                    Users = _mapper.Map<List<UserDTO>>(x.Select(y => y.User).OrderByDescending(z => z.RegisteredAt).ToList())
                });

            return result;
        }


        public IEnumerable<UserTasksDTO> GetSortedUsers() // TASK 5
        {
            var result = db.Users.Get()
                .GroupJoin(db.Tasks.Get(), u => u.Id, t => t.PerformerId, (u, t) => new UserTasksDTO
                {
                    User = _mapper.Map<UserDTO>(u),
                    Tasks = _mapper.Map<List<TaskDTO>>(t.OrderByDescending(t => t.Name.Length).ToList())
                })
               .OrderBy(x => x.User.FirstName);

            return result;
        }


        public UserInfoDTO GetUserLastProjectInfo(int userId) // Task 6
        {
            var result = db.Users.Get(u => u.Id == userId)
                .GroupJoin(db.Projects.Get(), u => u.Id, p => p.AuthorId, (u, p) => new { User = u, LastProject = p.OrderBy(p => p.CreatedAt).LastOrDefault() })
                .Select(x => new UserInfoDTO
                {
                    User = _mapper.Map<UserDTO>(x.User),
                    LastProject = _mapper.Map<ProjectDTO>(x.LastProject),
                    TasksLastProject = db.Tasks.Get(t => t.ProjectId == x.LastProject?.Id).Count(),
                    NotComletedTasks = db.Tasks.Get(t => t.PerformerId == x.User.Id && t.State != 2).Count(),
                    MaxTask = _mapper.Map<TaskDTO>(db.Tasks.Get(t => t.PerformerId == x.User.Id).OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault())
                }).FirstOrDefault();

            return result;
        }


        public IEnumerable<ProjectShortInfo> GetProjectShortInfo() // Task 7
        {
            var result = db.Projects.Get()
                .GroupJoin(db.Tasks.Get(), p => p.Id, t => t.ProjectId, (p, t) => new { Project = p, Tasks = t })
                .Select(x => new ProjectShortInfo
                {
                    Project = _mapper.Map<ProjectDTO>(x.Project),
                    LongDescriptionTask = _mapper.Map<TaskDTO>(x.Tasks.OrderBy(t => t.Description.Length).LastOrDefault()),
                    ShortestNameTask = _mapper.Map<TaskDTO>(x.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                    UserCount = (x.Project.Description.Length > 20 || x.Tasks.Count() < 3) ? db.Users.Get().Count(u => u.Team?.Id == x.Project.TeamId) : 0
                });

            return result;
        }

    }
}
