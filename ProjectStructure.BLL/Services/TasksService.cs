﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class TasksService : ITasksService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public TasksService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }
        public void CreateTask(TaskDTO item)
        {
            var task = _mapper.Map<TaskDTO, Task>(item);

            if (item.ProjectId != null)
                task.Project = db.Projects.FindById((int)item.ProjectId) ?? throw new ArgumentException($"Project with Id {item.ProjectId} doesn't exist!");
            if (item.PerformerId != null)
                task.Performer = db.Users.FindById((int)item.PerformerId) ?? throw new ArgumentException($"User with Id {item.PerformerId} doesn't exist!");
            if (item.TaskStateId != null)
                task.TaskState = db.TaskStates.FindById((int)item.TaskStateId) ?? throw new ArgumentException($"Task with Id {item.TaskStateId} doesn't exist!");

            db.Tasks.Create(task);
            db.Save();
        }

        public TaskDTO FindTaskById(int id)
        {
            var entity = db.Tasks.FindById(id);
            return _mapper.Map<Task, TaskDTO>(entity);
        }

        public IEnumerable<TaskDTO> GetTasks()
        {
            var entities = db.Tasks.Get();
            return _mapper.Map<IEnumerable<Task>, List<TaskDTO>>(entities);
        }

        public void RemoveTask(int id)
        {
            var task = db.Tasks.FindById(id);

            if (task == null)
                throw new ArgumentNullException($"Id {id} not found");

            db.Tasks.Remove(task);
            db.Save();
        }

        public void UpdateTask(TaskDTO item)
        {
            var task = _mapper.Map<TaskDTO, Task>(item);

            db.Tasks.Update(task);
            db.Save();
        }
    }
}
