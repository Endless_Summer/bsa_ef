﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public UsersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            db = unitOfWork;
        }

        public void CreateUser(UserDTO item)
        {
            var user = _mapper.Map<UserDTO, User>(item);
            if (item.TeamId != null)
                user.Team = db.Teams.FindById((int)item.TeamId) ?? throw new ArgumentException($"Team with Id {item.TeamId} doesn't exist!");
            db.Users.Create(user);

            db.Save();
        }

        public UserDTO FindUserById(int id)
        {
            var entity = db.Users.FindById(id);
            return _mapper.Map<User, UserDTO>(entity);
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            var entities = db.Users.Get();

            return _mapper.Map<IEnumerable<User>, List<UserDTO>>(entities);
        }

        public void RemoveUser(int id)
        {
            var user = db.Users.FindById(id) ?? throw new ArgumentNullException($"Id {id} not found");

            db.Users.Remove(user);
            db.Save();
        }

        public void UpdateUser(UserDTO item)
        {
            var user = _mapper.Map<UserDTO, User>(item);
           // user.Team = item.TeamId != null ? db.Teams.FindById((int)item.TeamId) ?? throw new ArgumentException($"Team with Id {item.TeamId} doesn't exist!") : null;
            db.Users.Update(user);
            db.Save();
        }
    }
}
