﻿namespace ProjectStructure.Common.Models
{
    public class ProjectShortInfo
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongDescriptionTask { get; set; }
        public TaskDTO ShortestNameTask { get; set; }
        public int UserCount { get; set; }
    }
}
