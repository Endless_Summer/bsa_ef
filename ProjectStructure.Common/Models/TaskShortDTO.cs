﻿namespace ProjectStructure.Common.Models
{
    public class TaskShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
