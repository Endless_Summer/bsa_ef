﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ProjectStructure.DAL
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
               .HasOne(p => p.Team)
               .WithMany(t => t.Users)
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany(t => t.Projects)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Projects)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Task>()
                .HasOne(p => p.TaskState)
                .WithMany(t => t.Tasks)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Task>()
                .HasOne(p => p.Performer)
                .WithMany(t => t.Tasks)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Task>()
                .HasOne(p => p.Project)
                .WithMany(t => t.Tasks)
                .OnDelete(DeleteBehavior.SetNull);

            // project constraints
            modelBuilder.Entity<Project>().Property(b => b.Name)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Project>().Property(b => b.Description)
                .HasDefaultValue("default project description")
                .HasMaxLength(500);

            modelBuilder.Entity<Project>().Property(b => b.CreatedAt)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<Project>().Property(b => b.Deadline)
                .HasDefaultValue(DateTime.Now.AddMonths(3));

            // task constraints
            modelBuilder.Entity<Task>().Property(b => b.Name)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Task>().Property(b => b.Description)
                .HasDefaultValue("default task description")
                .HasMaxLength(500);

            modelBuilder.Entity<Task>().Property(b => b.CreatedAt)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<Task>().Property(b => b.FinishedAt)
                .HasDefaultValue(DateTime.Now.AddMonths(1));

            // taskstate constraints
            modelBuilder.Entity<TaskState>().Property(b => b.Value)
                .IsRequired()
                .HasMaxLength(20);

            // team constraints
            modelBuilder.Entity<Team>().Property(b => b.Name)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Team>().Property(b => b.CreatedAt)
                .HasDefaultValue(DateTime.Now);

            // user constraints
            modelBuilder.Entity<User>().Property(b => b.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<User>().Property(b => b.LastName)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<User>().Property(b => b.Email)
                .HasMaxLength(100);

            modelBuilder.Entity<User>().Property(b => b.RegisteredAt)
                .HasDefaultValue(DateTime.Now);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskState>().HasData(GenerateData<TaskState>("Assets\\taskstates.json"));
            modelBuilder.Entity<User>().HasData(GenerateData<User>("Assets\\users.json"));
            modelBuilder.Entity<Team>().HasData(GenerateData<Team>("Assets\\teams.json"));
            modelBuilder.Entity<Project>().HasData(GenerateData<Project>("Assets\\projects.json"));
            modelBuilder.Entity<Task>().HasData(GenerateData<Task>("Assets\\tasks.json"));
        }

        private static List<T> GenerateData<T>(string file)
        {
            var entities = new List<T>();

            using (FileStream fs = new FileStream(file, FileMode.OpenOrCreate))
            {
                entities = JsonSerializer.DeserializeAsync<List<T>>(fs).Result;
            }

            return entities;
        }


    }
}
