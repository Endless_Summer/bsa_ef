﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.DAL.Entities
{
    public class Task : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public int? State { get; set; }
        //public int? TaskStateId { get; set; }

        [ForeignKey("State")]
        public virtual TaskState TaskState { get; set; }
        public int? ProjectId { get; set; }
        public virtual Project Project { get; set; }
        public int? PerformerId { get; set; }
        public virtual User Performer { get; set; }

    }
}
