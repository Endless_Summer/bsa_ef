﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.DAL.Entities
{
    public class TaskState : IEntity
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public virtual List<Task> Tasks { get; set; }
    }
}
