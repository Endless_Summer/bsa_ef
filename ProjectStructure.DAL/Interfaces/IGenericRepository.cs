﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get();
        TEntity FindById(int id);
        void Create(TEntity item);
        void Remove(TEntity item);
        void Update(TEntity item);
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);

    }


}
