﻿using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL
{
    public interface IUnitOfWork
    {
        IGenericRepository<User> Users { get; }
        IGenericRepository<Team> Teams { get; }
        IGenericRepository<TaskState> TaskStates { get; }
        IGenericRepository<Task> Tasks { get; }
        IGenericRepository<Project> Projects { get; }

        void Save();

    }
}
