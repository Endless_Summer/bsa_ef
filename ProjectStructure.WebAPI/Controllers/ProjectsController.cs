﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;
        public ProjectsController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }
        // GET: api/Projects
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            return Ok(_projectsService.GetProjects());
        }

        // GET api/Projects/5
        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            ProjectDTO result = _projectsService.FindProjectById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        // POST api/Projects
        [HttpPost]
        public ActionResult Post([FromBody] ProjectDTO value)
        {
            _projectsService.CreateProject(value);
            return Ok();
        }

        // PUT api/Projects/5
        [HttpPut]
        public ActionResult Put([FromBody] ProjectDTO value)
        {
            try
            {
                _projectsService.UpdateProject(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/Projects/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _projectsService.RemoveProject(id);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return NoContent();
        }
    }
}
