﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        readonly ITaskStateService _taskStateService;
        public TaskStatesController(ITaskStateService taskStateService)
        {
            _taskStateService = taskStateService;
        }

        // GET: api/TaskStates
        [HttpGet]
        public ActionResult<IEnumerable<TaskStateDTO>> Get()
        {
            return Ok(_taskStateService.GetTaskStates());
        }

        // GET api/TaskStates/5
        [HttpGet("{id}")]
        public ActionResult<TaskStateDTO> Get(int id)
        {
            TaskStateDTO result = _taskStateService.FindTaskStateById(id);

            if (result == null)
                return NotFound(null);

            return Ok(result);
        }

        // POST api/TaskStates>
        [HttpPost]
        public ActionResult Post([FromBody] TaskStateDTO value)
        {
            _taskStateService.CreateTaskState(value);
            return Ok();
        }

        // PUT api/TaskStates/5
        [HttpPut]
        public ActionResult Put([FromBody] TaskStateDTO value)
        {
            try
            {
                _taskStateService.UpdateTaskState(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/TaskStates/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _taskStateService.RemoveTaskState(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }
    }
}
