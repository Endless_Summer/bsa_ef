﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        readonly ITasksService _tasksService;
        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        // GET: api/Tasks
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            return Ok(_tasksService.GetTasks());
        }

        // GET api/Tasks/5
        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            TaskDTO result = _tasksService.FindTaskById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        // POST api/Tasks
        [HttpPost]
        public ActionResult Post([FromBody] TaskDTO value)
        {
            _tasksService.CreateTask(value);
            return Ok();
        }

        // PUT api/Tasks/5
        [HttpPut]
        public ActionResult Put([FromBody] TaskDTO value)
        {
            try
            {
                _tasksService.UpdateTask(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/Tasks/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _tasksService.RemoveTask(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }
    }
}
